package me.koenn.combatlog;

import me.koenn.combatlog.commands.CombatCommand;
import me.koenn.combatlog.listeners.CombatListener;
import me.koenn.combatlog.listeners.CommandListener;
import me.koenn.combatlog.listeners.PlayerLeaveListener;
import me.koenn.core.command.CommandAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class CombatLog extends JavaPlugin {

    private static CombatRegistry combatRegistry;
    private static Plugin plugin;

    public static Plugin getPlugin() {
        return plugin;
    }

    public static CombatRegistry getCombatRegistry() {
        return combatRegistry;
    }

    public void log(String msg) {
        this.getLogger().info(msg);
    }

    @Override
    public void onEnable() {
        plugin = this;
        log("All credits for this plugin go to Koenn");
        Bukkit.getPluginManager().registerEvents(new CombatListener(), this);
        Bukkit.getPluginManager().registerEvents(new CommandListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerLeaveListener(), this);
        combatRegistry = new CombatRegistry();
        CommandAPI.registerCommand(new CombatCommand(), this);
    }

    @Override
    public void onDisable() {
        log("All credits for this plugin go to Koenn");
    }
}