package me.koenn.combatlog.commands;

import me.koenn.combatlog.CombatLog;
import me.koenn.core.command.Command;
import me.koenn.core.player.CPlayer;
import net.md_5.bungee.api.ChatColor;

public class CombatCommand extends Command {

    public CombatCommand() {
        super("ct", "/ct");
    }

    @Override
    public boolean execute(CPlayer cPlayer, String[] strings) {
        if (CombatLog.getCombatRegistry().getPlayers().containsKey(cPlayer.getUUID())) {
            cPlayer.sendMessage(ChatColor.RED + "You are in combat for " + CombatLog.getCombatRegistry().getPlayer(cPlayer.getUUID()).getTimeLeft() + " seconds");
            return true;
        }
        cPlayer.sendMessage(ChatColor.GREEN + "You are not in combat!");
        return true;
    }
}
