package me.koenn.combatlog;

import me.koenn.combatlog.counter.CombatCounter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.HashMap;
import java.util.UUID;

public class CombatRegistry {

    private HashMap<UUID, CombatCounter> players = new HashMap<>();

    public void registerPlayer(UUID uuid) {
        if (!this.players.containsKey(uuid)) {
            CombatCounter timer = new CombatCounter();
            timer.start(() -> {
                this.unregisterPlayer(uuid);
                try {
                    Bukkit.getPlayer(uuid).sendMessage(ChatColor.GREEN + "You are now out of combat!");
                } catch (Exception ex) {
                    CombatLog.getPlugin().getLogger().info("Player '" + uuid + "' left in combat!");
                }
            });
            this.players.put(uuid, timer);
        }
    }

    public void unregisterPlayer(UUID uuid) {
        if (this.players.containsKey(uuid)) {
            this.players.remove(uuid);
        }
    }

    public CombatCounter getPlayer(UUID uuid) {
        if (this.players.containsKey(uuid)) {
            return this.players.get(uuid);
        }
        return null;
    }

    public HashMap<UUID, CombatCounter> getPlayers() {
        return players;
    }
}
