package me.koenn.combatlog.counter;

import me.koenn.combatlog.CombatLog;
import me.koenn.core.misc.Counter;

public class CombatCounter extends Counter {

    public CombatCounter() {
        super(30, CombatLog.getPlugin());
    }

    @Override
    public void onCount(int timeLeft) {

    }
}
