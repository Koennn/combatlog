package me.koenn.combatlog.listeners;

import me.koenn.combatlog.CombatLog;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class CombatListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDamage(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) {
            return;
        }

        if (event.isCancelled()) {
            return;
        }

        Player player1 = (Player) event.getEntity();
        Player player2 = (Player) event.getDamager();
        player1.setAllowFlight(false);
        player2.setAllowFlight(false);
        CombatLog.getCombatRegistry().registerPlayer(player1.getUniqueId());
        CombatLog.getCombatRegistry().registerPlayer(player2.getUniqueId());
        player1.sendMessage(ChatColor.RED + "You are now in combat!");
        player2.sendMessage(ChatColor.RED + "You are now in combat!");
    }
}
