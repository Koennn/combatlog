package me.koenn.combatlog.listeners;

import me.koenn.combatlog.CombatLog;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerLeaveListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (CombatLog.getCombatRegistry().getPlayers().containsKey(event.getPlayer().getUniqueId())) {
            CombatLog.getCombatRegistry().getPlayers().remove(event.getPlayer().getUniqueId());
            event.getPlayer().sendMessage(ChatColor.RED + "You're not allowed to execute commands while in combat!");
            for (ItemStack item : event.getPlayer().getInventory().getContents()) {
                if (item != null && !item.getType().equals(Material.AIR)) {
                    event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), item);
                }
            }
            for (ItemStack item : event.getPlayer().getInventory().getArmorContents()) {
                if (item != null && !item.getType().equals(Material.AIR)) {
                    event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), item);
                }
            }
            event.getPlayer().getInventory().clear();
            event.getPlayer().getInventory().setArmorContents(new ItemStack[4]);
            event.getPlayer().setHealth(0);
        }
    }
}
