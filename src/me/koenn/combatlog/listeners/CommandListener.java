package me.koenn.combatlog.listeners;

import me.koenn.combatlog.CombatLog;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandListener implements Listener {

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().equalsIgnoreCase("/ct")) {
            return;
        }
        if (CombatLog.getCombatRegistry().getPlayers().containsKey(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + "You're not allowed to execute commands while in combat!");
        }
    }
}
